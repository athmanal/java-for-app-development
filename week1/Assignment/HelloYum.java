/**
    * A class HelloYum that prints info.
    * @version 1.0
    * @author: Athma Narayanan Lakshminarayanan athmanal
    * All rights reserved
 */
public class HelloYum {
    public static void main(String[] args) {
        System.out.println("Andrew ID: athmanal");
        System.out.println("First Name: Athma Narayanan");
        System.out.println("Last Name: Lakshminarayanan");
        System.out.println("Favorite Restaurant: Cheesecake Factory");
        System.out.println("Best Dish: Oreo Cheescake");
    }
}
