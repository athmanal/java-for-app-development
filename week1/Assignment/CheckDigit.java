/**
    * A class CheckDigit that prints UPC 12th digit.
    * @version 1.0
    * @author: Athma Narayanan Lakshminarayanan athmanal
    * All rights reserved
 */
public class CheckDigit {
    public static void main(String[] args) {
        long m = args.length;
        long a = Long.parseLong(args[0].substring(0 , 1));
        long b = Long.parseLong(args[0].substring(1 , 2));
        long c = Long.parseLong(args[0].substring(2 , 3));
        long d = Long.parseLong(args[0].substring(3 , 4));
        long e = Long.parseLong(args[0].substring(4 , 5));
        long f = Long.parseLong(args[0].substring(5 , 6));
        long g = Long.parseLong(args[0].substring(6 , 7));
        long h = Long.parseLong(args[0].substring(7 , 8));
        long i = Long.parseLong(args[0].substring(8 , 9));
        long j = Long.parseLong(args[0].substring(9 , 10));
        long k = Long.parseLong(args[0].substring(10 , 11));
        long l;
        l = (10 - (3 * a + b + 3 * c + d + 3 * e + f + 3 * g + h + 3 * i + j + 3 * k) % 10) % 10;
        System.out.println("" + a + b + c + d + e + f + g + h + i + j + k);
        System.out.println(+l);
    }
}
