/**
    * A class for Octagon.
    * @version 1.0
    * @author: Athma Narayanan Lakshminarayanan athmanal
    * All rights reserved
 */
public class Octagon extends Shape {
    /**
     * side of Octagon.
     */
    private double side;
    /**
     * Returns area of shape object.
     *@param Nil
     * @return area value of shape object
     */
    public  double getArea() {
        return 2 * (1 + Math.pow(2 , 0.5)) * (Math.pow(side , 2));
    }
    /**
     * Returns perimeter of shape object.
     *@param Nil
     * @return perimeter value of shape object
     */
    public  double getPerimeter() {
        return 8 * side;
    }
    /**
     * Constructor with side.
     * @param newSide new Side of Octagon
     */
    public Octagon(double newSide) {
        side = newSide;
    }
    /**
     * Returns Side of Octagon.
     **@param Nil
     * @return Side of Octagon
     */
    public double getSide() {
        return side;
    }
    /**
     * Returns string representation of Octagon object.
     * @return a string representation of Octagon object
     */
    public String toString() {
        return String.format("Octagon %3.3f %3.3f" , getArea() , getPerimeter());
    }
}
