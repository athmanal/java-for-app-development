/**
    * A class for Square.
    * @version 1.0
    * @author: Athma Narayanan Lakshminarayanan athmanal
    * All rights reserved
 */
public class Square extends Rectangle {
    /**
    * Side value of square.
    */
    private double side;
    /**
     * Returns area of shape object.
    *@param Nil
    * @return area value of shape object
    */
    public  double getArea() {
        return side * side;
    }
    /**
     * Returns perimeter of shape object.
     *@param Nil
     * @return perimeter value of shape object
     */
    public  double getPerimeter() {
        return 4 * side;
    }
    /**
     * Constructor with new side.
     *@param Nil
     * @param newSide new side value of square
     */
    public Square(double newSide) {
        super(newSide , newSide);
        side = newSide;
    }
    /**
     * Returns side of square object.
     *@param Nil
     * @return side value of square object
     */
    public double getSide() {
        return side;
    }
    /**
     * Returns string representation of Square object.
     * @return a string representation of Square object
     */
    public String toString() {
        String b = String.format("Square %3.3f %3.3f" , getArea() , getPerimeter());
        return b;
    }
}
