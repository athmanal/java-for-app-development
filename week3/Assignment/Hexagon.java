/**
* A class for Hexagon.
* @version 1.0
* @author: Athma Narayanan Lakshminarayanan athmanal
* All rights reserved
*/
public class Hexagon extends Shape {
    /**
     * Side of Hexagon.
     */
    private double side;
    /**
     * Returns area of shape object.
     *@param Nil
     * @return area value of shape object
     */
    public  double getArea() {
        return 1.5 * (Math.pow(3 , 0.5)) * (Math.pow(side , 2));
    }
    /**
     * Returns perimeter of shape object.
     *@param Nil
     * @return perimeter value of shape object
     */
    public  double getPerimeter() {
        return 6 * side;
    }
    /**
     * Constructor with side.
     * @param newSide new Side of Hexagon
     */
    public Hexagon(double newSide) {
        side = newSide;
    }

    /**
     * Returns Side of Hexagon.
     **@param Nil
     * @return Side of Hexagon
     */
    public double getSide() {
        return side;
    }
    /**
     * Returns string representation Hexagon object.
     * @return a string representation of Hexagon object
     */
    public String toString() {
        return String.format("Hexagon %3.3f %3.3f", getArea() , getPerimeter());
    }
}
