/**
    * A class for Sorting the shapes.
    * @version 1.0
    * @author: Athma Narayanan Lakshminarayanan athmanal
    * All rights reserved
 */
public class ShapeSortTest {
    /**
     * Returns sorted answers.
     *@param args input
     */
    public static void main(String[] args) {
        int  name;
        long value;
        Shape[] ha = new Shape[args.length];
        /*
            *Look Through inputs and figure out what the
            *object is based on C/S/H/O
        */
        for (int i = 0; i < args.length; i++) {
            name = args[i].charAt(0);
            value = Long.parseLong(args[i].substring(1 , args[i].length()));
            switch (name) {
                case 'S':
                    ha[i] = new Square(value);
                    break;
                case 'C':
                    ha[i] = new Circle(value);
                    break;
                case 'H':
                    ha[i] = new Hexagon(value);
                    break;
                case 'O':
                    ha[i] = new Octagon(value);
                    break;
                default:
                    System.out.println("???");
            }
        }
        /*
            *Ascending Sort
            *And Print
        */
        for (int i = 0; i < ha.length - 1; i++) {
            for (int j = 0; j < ha.length - i - 1; j++) {
                if (ha[j].getArea() > ha[j + 1].getArea()) {
                    Shape tempVar = ha [j];
                    ha [j] = ha [j + 1];
                    ha [j + 1] = tempVar;
                }
            }
        }
        for (int i = 0; i < ha.length; i++) {
            System.out.printf(ha[i] + "\n");
        }
        /*
            *Descending Sort
            *And Print
        */
        System.out.printf("\n");
        for (int i = 0; i < ha.length - 1; i++) {
            for (int j = 0; j < ha.length - i - 1; j++) {
                if (ha[j].getPerimeter() < ha[j + 1].getPerimeter()) {
                    Shape tempVar = ha [j];
                    ha [j] = ha [j + 1];
                    ha [j + 1] = tempVar;
                }
            }
        }
        for (int i = 0; i < ha.length; i++) {
            System.out.printf(ha[i] + "\n");
        }
    }
}
