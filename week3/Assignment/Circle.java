/**
* A class for Circle.
* @version 1.0
* @author: Athma Narayanan Lakshminarayanan athmanal
* All rights reserved
*/
public class Circle extends Shape {
    /**
     * radius of a circle.
     */
    private double radius;
    /**
     * Returns area of shape object.
     *@param Nil
     * @return area value of shape object
     */
    public double getArea() {
        return Math.PI * radius * radius;
    }
    /**
     * Returns perimeter of shape object.
     *@param Nil
     * @return perimeter value of shape object
     */
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }
    /**
     * Constructor with radius.
     *
     * @param newRadius radius for a new circle
     */
    public Circle(double newRadius) {
        radius = newRadius;
    }

    /**
     * Returns radius of a circle.
     *@param Nil
     * @return radius value of a circle
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Returns string representation of circle object.
     *@param Nil
     * @return a string representation of circle object
     */
    public String toString() {
        return String.format("Circle %3.3f %3.3f", getArea() , getPerimeter());
    }
}
