/**
    * A superclass all shapes.
    * @version 1.0
    * @author: Athma Narayanan Lakshminarayanan athmanal
    * All rights reserved
 */
public abstract class  Shape {
    /**
     * Constructor with new area.
     * @param newArea new Area of shape
     *@param newPerimeter new Perimeter of shape
    */
//    public Shape();

    /**
     * Returns area of shape object.
     *@param Nil
     * @return area value of shape object
     */
    public abstract double getArea();
    /**
     * Returns perimeter of shape object.
     *@param Nil
     * @return perimeter value of shape object
     */
    public abstract double getPerimeter();
}
