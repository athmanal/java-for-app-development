
public class IsOdd2 {
	public static boolean isOdd(int i) {
		return i % 2 != 0;
	}

	public static void main(String[] args) {
        int x = Integer.parseInt(args[0]);
        System.out.println(isOdd(x));
    }

}
