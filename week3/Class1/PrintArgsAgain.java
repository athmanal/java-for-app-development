public class PrintArgsAgain {
    public static void main(String[] args) {
        // Print out the arguments
        for (String s : args) {
            System.out.println(s);
        }
    }
}
