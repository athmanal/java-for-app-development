/**
    * A superclass all students.
    * @version 1.0
    * @author: Athma Narayanan Lakshminarayanan athmanal
    * All rights reserved
 */
public class  Student{
    /**
     * Student's first name.
     */
    private String firstname;
    /**
     * Student's last name..
     */
    private String lastname;
    /**
     * Student's ID..
     */
    private String andrewId;
    /**
     * Student's phone number..
     */
    private String phonenumber;
    /**
     * Constructor with newid.
     * @param andrewid new Andrew ID
    */
    public Student(String andrewid) {
        andrewId = andrewid;
    }
    /**
     * Returns andrewid of student.
     *@param Nil
     * @return andrewId
     */
    public String getAndrewId() {
        return andrewId;
    }
     /**
     * Returns firstname of student.
     *@param Nil
     * @return firstname
     */
    public String getFirstName() {
        return firstname;
    }
    /**
     * Returns lastname of student.
     *@param Nil
     * @return lastname
     */
    public String getLastName() {
        return lastname;
    }
    /**
     * Returns phno of student.
     *@param Nil
     * @return phno
     */
    public String getPhoneNumber() {
        return phonenumber;
    }
///////////////////////////
    /**
     * Sets firstname of student.
     *@param s input string.
     */
    public void setFirstName(String s) {
        firstname=s;
    }
    /**
     * Sets lastname of student.
     *@param s input string.
     */
    public void setLastName(String s) {
        lastname=s;
    }
    /**
     * Sets phno of student.
     *@param s input string.
     */
    public void setPhoneNumber(String s) {
        phonenumber=s;
    }

    /**
     * Returns strirepresentation of shape object.
     * @return a string representation of shape object
     */
    public String toString() {
        String a = String.format("%s %s (Andrew ID: %s,    Phone   Number: %s",getFirstName(),getLastName(),getAndrewId(),getPhoneNumber());
        return a;
    }
}
