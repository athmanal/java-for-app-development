/**
    * A class for Hexagon.
    * @version 1.0
    * @author: Athma Narayanan Lakshminarayanan athmanal
    * All rights reserved
 */
public class Hexagon extends Shape {
    /**
     * Side of Hexagon.
     */
    private double side;

    /**
     * Constructor with side.
     * @param newSide new Side of Hexagon
     */
    public Hexagon(double newSide) {
        super(1.5 * (Math.pow(3 , 0.5)) * (Math.pow(newSide , 2)) , 6 * newSide);
        side = newSide;
    }

    /**
     * Returns Side of Hexagon.
     **@param Nil
     * @return Side of Hexagon
     */
    public double getSide() {
        return side;
    }
   /**
     * Returns string representation Hexagon object.
     * @return a string representation of Hexagon object
     */
    public String toString() {
        return String.format("Hexagon %3.3f %3.3f", getArea() , getPerimeter());
    }
}
