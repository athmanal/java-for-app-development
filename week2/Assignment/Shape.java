/**
    * A superclass all shapes.
    * @version 1.0
    * @author: Athma Narayanan Lakshminarayanan athmanal
    * All rights reserved
 */
public class Shape {
    /**
     * area of Shape.
     */
    private double area;
    /**
     * perimeter of Shape.
     */
    private double perimeter;

    /**
     * Constructor with new area.
     * @param newArea new Area of shape
     *@param newPerimeter new Perimeter of shape
    */
    public Shape(double newArea, double newPerimeter) {
        area = newArea;
        perimeter = newPerimeter;

    }
    /**
     * Returns area of shape object.
     *@param Nil
     * @return area value of shape object
     */
    public double getArea() {
        return area;
    }
    /**
     * Returns perimeter of shape object.
     *@param Nil
     * @return perimeter value of shape object
     */
    public double getPerimeter() {
        return perimeter;
    }

    /**
     * Returns string representation of shape object.
     * @return a string representation of shape object
     */
    public String toString() {
        String a = String.format("Shape %3.3f %3.3f" , getArea() , getPerimeter());
        return a;
    }
}
