/**
 * Example class to represent a circle shape.
 *
 * @author  Athma Narayanan, athmanal
 *
 */
public class Octagon extends Shape {
    /**
     * radius of a circle.
     */
    public double side;

    /**
     * Constructor with radius.
     *
     * @param newRadius
     *            radius for a new circle
     */
    public Octagon(double newSide) {
        super(2*(1+Math.pow(2,0.5))*(Math.pow(newSide,2)),8*newSide);
        side = newSide;
    }

    /**
     * Returns radius of a circle.
     *
     * @return radius value of a circle
     */
    public double getSide() {
        return side;
    }

    /**
     * Returns string representation of circle object.
     * @return a string representation of circle object
     */
    public String toString() {
        System.out.printf("Octagon %3.3f  %3.3f ",area,perimeter);
        return "Octagon(side=" + side + ")";
    }
}
