/**
 * Example class to represent a circle shape.
 *
 * @author  Athma Narayanan, athmanal
 *
 */
public class Hexagon extends Shape {
    /**
     * radius of a circle.
     */
    public double side;

    /**
     * Constructor with radius.
     *
     * @param newRadius
     *            radius for a new circle
     */
    public Hexagon(double newSide) {
        super(1.5*(Math.pow(3,0.5))*(Math.pow(newSide,2)),6*newSide);
        side = newSide;
    }

    /**
     * Returns radius of a circle.
     *
     * @return radius value of a circle
     */
    public double getSide() {
        return side;
    }

    /**
     * Returns string representation of circle object.
     * @return a string representation of circle object
     */
    public String toString() {
        System.out.printf("Hexagon %3.3f  %3.3f ",area,perimeter);
        return "Hexagon(side=" + side + ")";
    }
}
