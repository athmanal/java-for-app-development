/**
 * Example class to represent a rectangle shape.
 *
 * @author  Athma Narayanan, athmanal
 *
 */
public class Rectangle extends Shape {
    /**
     * width of a Rectangle.
     */
    public double width;
    /**
     * height of a Rectangle.
     */
    public double height;

    /**
     * Constructor with new width and new height.
     * @param newWidth new width of Rectangle
     * @param newHeight new height of Rectangle
     */
    public Rectangle(double newWidth, double newHeight) {
        super(newWidth * newHeight,2*(newWidth + newHeight));
        width = newWidth;
        height = newHeight;
    }

    /**
     * Returns height of a rectangle object.
     * @return height value of rectangle object
     */
    public double getHeight() {
        return height;
    }

    /**
     * Returns width of a rectangle object.
     * @return width value of rectangle object
     */
    public double getWidth() {
        return width;
    }

    /**
     * Returns string representation of Rectangle object.
     * @return a string representation of rectangle object
     */
    public String toString() {
        System.out.printf("Rectangle<%f><%f>",area,perimeter);
        return "Rectangle(width=" + width + ", height=" + height + ")";
    }
}
