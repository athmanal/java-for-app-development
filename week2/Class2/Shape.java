/**
 * The super class for all shapes.
 *
 * @author  Athma Narayanan, athmanal
 */
public class Shape {
    /**
     * area of Shape.
     */
    public double area;
    public double perimeter;

    /**
     * Constructor with new area.
     * @param newArea new Area of shape
     */
        public Shape(double newArea, double newPerimeter) {
        area = newArea;
        perimeter=newPerimeter;

    }

    /**
     * Returns area of shape object.
     * @return area value of shape object
     */
    public double getArea() {
        return area;
    }

        /**
     * Returns perimeter of shape object.
     * @return perimeter value of shape object
     */
    public double getPerimeter() {
        return perimeter;
    }

    /**
     * Returns string representation of shape object.
     * @return a string representation of shape object
     */
    public String toString() {
       System.out.printf("Shape %3.3f  %3.3f ",area,perimeter);
        return "Shape(area=" + area + ")";
    }
}
